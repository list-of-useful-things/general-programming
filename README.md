# Libraries/frameworks
* [A list of open source C++ libraries](https://en.cppreference.com/w/cpp/links/libs)
* [Bond](https://microsoft.github.io/bond/) - Bond is an open source, cross-platform framework for working with schematized data. It supports cross-language serialization/deserialization and powerful generic mechanisms for efficiently manipulating data. Bond is broadly used at Microsoft in high scale services.
* [Clipper](http://www.angusj.com/delphi/clipper.php) - an open source freeware library for clipping and offsetting lines and polygons (Delphi, C++ and C#)

# Various
* [curl command examples](https://linuxhandbook.com/curl-command-examples/)
* [public apis](https://public-apis.xyz/)
* [Data Structure Visualizations](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html)
* [HTTP/3 Explained](https://http3-explained.haxx.se/en/)
* [30 seconds of knowledge](https://30secondsofknowledge.petrovicstefan.rs/)
* [Code Shelter](https://www.codeshelter.co/)

# Books
* [the book of secret knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
* [Open Book Shelf](https://launchschool.com/books)
* [Open Book Project](http://www.openbookproject.net/books/)

# Data structures
* [Bloom filter](https://en.wikipedia.org/wiki/Bloom_filter)
* [Trie](https://en.wikipedia.org/wiki/Trie)
* [Circular buffer](https://en.wikipedia.org/wiki/Circular_buffer)
* [Merkle Trees](https://ordepdev.me/posts/diving-into-merkle-trees)
 
# Projects
* [Node-RED](https://nodered.org/) - Flow-based programming for the Internet of Things
* [CodeSandbox](https://codesandbox.io/) - online code editor
* [Pi-hole](https://www.balena.io/blog/deploy-network-wide-ad-blocking-with-pi-hole-and-a-raspberry-pi/) - network-wide ad-blocking with Pi-hole and a Raspberry Pi
* [Awesome list of open source applications for macOS.](https://github.com/serhii-londar/open-source-mac-os-apps)
* [n8n.io](https://n8n.io/) - Open Source Alternative for Zapier/tray.io (Automation tool)

### HTML UI Desktop app frameworks
* Close Source
  * [Ultralight](https://ultralig.ht/) - rendering HTML UI within games and desktop apps / [github](https://github.com/ultralight-ux/ultralight)
  * [sciter](https://sciter.com/prices/)
* Open Source
  * [electrino](https://github.com/pojala/electrino) - an experimental featherweight alternative to the popular and powerful Electron
  * [Chromely](https://github.com/chromelyapps/Chromely) - a lightweight alternative to Electron.NET, Electron for .NET/.NET Core developers
  * [webview](https://github.com/zserge/webview) - a tiny cross-platform webview library for C/C++/Golang
  * [Neutralinojs](https://neutralino.js.org/)
  * [appjs](http://appjs.com/)

# News / Blogs
* [hackernoon](https://hackernoon.com/)
* [The Hacker News](https://thehackernews.com/)
* [DZone](https://dzone.com/)
* [Tech meme](https://www.techmeme.com/)
* [InfoQ](https://www.infoq.com/)
* [Robert C. Martin (Uncle Bob) Blog](http://blog.cleancoder.com/)
* [Geeks For Geeks](https://www.geeksforgeeks.org/)
* [Discover dev](https://www.discoverdev.io/)
* [PL / Forever Frame](https://foreverframe.net/)
 
# Courses/Conference videos etc
* [exercism.io](https://exercism.io/)
* [edx](https://www.edx.org/)
* [dev.tube](https://dev.tube/)
* [MIT OpenCourseWare](https://ocw.mit.edu/courses/find-by-topic/)
* [Teach Yourself Computer Science](https://teachyourselfcs.com/)
* [Google Tech Talks](https://www.youtube.com/channel/UCtXKDgv1AVoG88PLl8nGXmw)
* [Awesome Talks](https://awesometalks.party/categories)
* [2uts](https://2uts.com/) - over 74,500 video tutorials
 
# Articles
* [UX / 7 Rules for Creating Gorgeous UI](https://medium.com/@erikdkennedy/7-rules-for-creating-gorgeous-ui-part-1-559d4e805cda)
* [Interview / 50+ Data Structure and Algorithms Interview Questions for Programmers](https://hackernoon.com/50-data-structure-and-algorithms-interview-questions-for-programmers-b4b1ac61f5b0)
* [Interview / Acing the Google Interview: The Ultimate Guide](https://www.byte-by-byte.com/google-interview/)
* [General / Concurrency and parallelism are two different things](https://luminousmen.com/post/concurrency-and-parallelism-are-different)
* [General / Asynchronous programming. Blocking I/O and non-blocking I/O](https://luminousmen.com/post/asynchronous-programming-blocking-and-non-blocking)
* [General / Crafting a Command Line Experience that Developers Love](https://codeburst.io/crafting-a-command-line-experience-that-developers-love-68657b20c28d)
* [General / File descriptor](https://www.computerhope.com/jargon/f/file-descriptor.htm)
* [General / Monospaced Programming Fonts with Ligatures](https://www.hanselman.com/blog/MonospacedProgrammingFontsWithLigatures.aspx)
* [General / Laws, Theories, Principles and Patterns that developers will find useful](https://github.com/dwmkerr/hacker-laws)
* [General / 10 tips for reviewing code you don’t like](https://developers.redhat.com/blog/2019/07/08/10-tips-for-reviewing-code-you-dont-like/)
* [General / How To Build An App: Everything You Didn't Know You Needed To Know](https://www.youtube.com/playlist?list=PL96C35uN7xGJu6skU4TBYrIWxggkZBrF5)
* [General / How I encrypt my data in the cloud](https://www.robertclarke.com/cloud-encryption/)
* [API / The Complexity of API Discovery](http://apievangelist.com/2019/07/01/the-complexity-of-api-discovery/)
* [Inversion of Control (Explained Non-Technically)](https://dzone.com/articles/inversion-of-control-explained-non-technically)
* [Business / Product Hunt 101: How To Launch Your Product From Early Idea To Revenue](https://medium.com/swlh/product-hunt-101-how-to-launch-your-product-from-early-idea-to-revenue-c3f01864cdde)
* [VAR / Things All Developers Should Learn In College](https://dev.to/taillogs/what-developers-should-actually-learn-in-college-2nen)
* [VAR / A collection of Vim key binds](https://medium.com/@sidneyliebrand/a-collection-of-vim-key-binds-4d227c9a455)
* [Design / UUID or GUID as Primary Keys? Be Careful!](https://tomharrisonjr.com/uuid-or-guid-as-primary-keys-be-careful-7b2aa3dcb439)

# Design
* [How To Make Your iOS App Typography NOT suck](https://uxdesign.cc/how-to-make-the-typography-of-your-ios-app-not-suck-a6de09fb7c41)

# Docker
* [Lazy Docker](https://github.com/jesseduffield/lazydocker) - The lazier way to manage everything docker
* [Fetching a private GitHub repository in CircleCI to build a Docker image](https://blog.lelonek.me/private-dependencies-from-github-in-your-docker-container-92e3b8cbf677)
* [Executable Images - How to Dockerize Your Development Machine](https://www.infoq.com/articles/docker-executable-images/)
* [Intro Guide to Dockerfile Best Practices](https://blog.docker.com/2019/07/intro-guide-to-dockerfile-best-practices/)
* [A comprehensive introduction to Docker, Virtual Machines, and Containers](https://medium.freecodecamp.org/comprehensive-introductory-guide-to-docker-vms-and-containers-4e42a13ee103)
* [NOT DOCKER / Goodbye Docker: Purging is Such Sweet Sorrow](https://zwischenzugs.com/2019/07/27/goodbye-docker-purging-is-such-sweet-sorrow/)
 
# Git
* [An Itroduction to Git Merge and Git Rebase: What They Do and When to Use Them](https://medium.freecodecamp.org/an-introduction-to-git-merge-and-rebase-what-they-are-and-how-to-use-them-131b863785f)
* [15 Git Commands You May Not Know](https://zaiste.net/15-git-commands-you-may-not-know/)
* [GIT EXPLAINED IN ONE HOUR](https://www.youtube.com/watch?v=BnDiKdG6oUA)

# Kubernetes
* [kubectl commands cheatsheet](https://medium.com/faun/kubectl-commands-cheatsheet-43ce8f13adfb)

# Algorithms
* [Data Structures and Algorithms Problems](https://www.techiedelight.com/list-of-problems/)
* [AlgoRythmics](https://www.youtube.com/user/AlgoRythmics/videos)

# Tools
* [Hex editor / hexyl](https://github.com/sharkdp/hexyl)
* [Terminal / conemu](https://conemu.github.io/)
* [Terminal / Microsoft terminal](https://github.com/Microsoft/Terminal)
* [Terminal / Cmder](https://cmder.net/) - Cmder is a software package created out of pure frustration over the absence of nice console emulators on Windows. It is based on amazing software, and spiced up with the Monokai color scheme and a custom prompt layout, looking sexy from the start
* [File Manager / Fucking Fast File-Manager](https://github.com/dylanaraps/fff)
* [File Manafer / nnn](https://github.com/jarun/nnn/)
* [Editor / VSCodium](https://github.com/VSCodium/vscodium)
* [Git / autogit](https://github.com/fabiospampinato/autogit)
* [Git / githistory](https://githistory.xyz/)
* [Video / Flowblade](https://jliljebl.github.io/flowblade/)
* [IM / Volt](https://volt.ws/)
* [VAR / Sherlock – A Tool To Find Usernames Across Social Networks](https://latesthackingnews.com/2019/01/30/sherlock-a-tool-to-find-usernames-across-social-networks/)
* [VAR / Awesome Design Tools](https://github.com/LisaDziuba/Awesome-Design-Tools)
* [VAR / Luna](https://luna-lang.org/)
* [VAR / List of open source tools](https://github.com/qarmin/Rewelacyjne-OpenSource)
* [VAR / List of open source tools](https://github.com/sindresorhus/awesome)
* [VAR / List of popular software safe alternatives](https://prism-break.org/en/all/)
* [Bootable sd/usb / Etcher](https://www.balena.io/etcher/)
* [Bootable sd/usb / Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)
* [remote connection manager / mRemoteNG](https://mremoteng.org/)
* [File formats / CSV to JSON](https://www.csvjson.com/csv2json)

# Online tools
* [Hex editor - online hex editor](http://onlinehexeditor.com)
* [Database - DBDiagram: A Relational Database Diagram Design Tool](https://dbdiagram.io/)
* [Diagram - Draw.io](https://www.draw.io/)
* [Java Thread Dump Analyzer](https://fastthread.io/)
* [Drama](https://www.drama.app/beta) - Prototyping interfaces for mobile applications

# Open source games
* <b>[Game Source Code Collection](https://archive.org/details/gamesourcecode)</b>
* [Tetris (C++/SFML)](https://github.com/FiendChain/Tetris)
* [Simon Tatham's Portable Puzzle Collection](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/)
* [Unix ASCII games](https://github.com/ligurio/awesome-ttygames)
* [Open Source Game Clones](https://osgameclones.com/) / [sources](https://github.com/opengaming/osgameclones)
* [Widelands](https://wl.widelands.org/)
* [Unknown Horizons](http://unknown-horizons.org)
* [0 A.D](https://play0ad.com/)
* [MEGAGLEST](https://megaglest.org/)
* [Open Imperium Galactica](https://github.com/akarnokd/open-ig)
* [OpenRA](http://www.openra.net/)
* [Spring: 1944](http://spring1944.net/)
* [Warzone 2100](http://wz2100.net/)
* [Glest](http://glest.org/en/index.php)
* [NetPanzer](http://www.netpanzer.info/)
* [Globulation 2](https://globulation2.org/wiki/Main_Page)
* [Zero-K](https://zero-k.info/)
* [Annex Conquer](http://annexconquer.com/)
* [Seven Kingdoms](https://7kfans.com/)
* [Evolution](https://www.evolutionrts.info/)

# GameDev
* [How to create a sprite sheet](https://www.codeandweb.com/texturepacker/tutorials/how-to-create-a-sprite-sheet)
* [Pixel Art Tutorial](http://makegames.tumblr.com/post/42648699708/pixel-art-tutorial)
* [2dgameartguru](https://2dgameartguru.com/)
* [Character Animation...](https://2dgameartguru.com/character-animation/)
* [Piskel - a free online editor for animated sprites & pixel art](https://www.piskelapp.com/)
* [Making retro-themed game](https://gamedevelopment.tutsplus.com/articles/going-old-school-making-games-with-a-retro-aesthetic--gamedev-3567)

# Network
* [Benchmarking nftables](https://developers.redhat.com/blog/2017/04/11/benchmarking-nftables/)
* [Beginners Guide to nftables Traffic Filtering](https://linux-audit.com/nftables-beginners-guide-to-traffic-filtering/)
* [Blokowanie niepożądanej komunikacji z nftables na linux](https://morfikov.github.io/post/blokowanie-niepozadanej-komunikacji-z-nftables-na-linux/)
* [Best way to filter/limit ARP packets on embedded Linux](https://unix.stackexchange.com/questions/512371/best-way-to-filter-limit-arp-packets-on-embedded-linux/517351#517351)
* [Pi-hole](https://pi-hole.net/) - Network-wide Ad Blocking

# Languages
### Rust
* [Rust Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/)
 
### Other Languages
* [V lang](https://github.com/vlang-io/v) / [website](http://vlang.io/)

# Wiki
* [loose/tight coupling](https://stackoverflow.com/questions/2832017/what-is-the-difference-between-loose-coupling-and-tight-coupling-in-the-object-o)

# Programming challenges
* [Codility](https://app.codility.com/programmers/lessons/1-iterations/)
* [Multi-threaded programming quizzes](https://deadlockempire.github.io/)
* [Unix Game](https://www.unixgame.io/unix50) - The Unix Game is a fun, low-barrier-to-entry programming contest where players solve coding challenges by constructing "pipelines" of UNIX text processing utilities to compute the solution

# Programming varia
* [Fira Code: monospaced font with programming ligatures](https://github.com/tonsky/FiraCode)
* [Simple Tricks To Speed Up Your Development](https://medium.com/@eliezer/simple-tricks-to-speed-up-your-development-2e7a5e64f141)

# Other 
* [Monte Carlo methods](https://easylang.online/apps/tutorial_mcarlo.html) - Why it's a bad idea to go to the casino
* [Tabs over Spaces](https://www.reddit.com/r/javascript/comments/c8drjo/nobody_talks_about_the_real_reason_to_use_tabs/)
* [No CS Degree](https://www.nocsdegree.com/) - Inspiring interviews with successful self-taught and bootcamp developers
* [Here Are Some of the Best Resources to Improve Your Coding Skills](https://medium.com/better-programming/here-are-some-of-the-best-resources-to-improve-your-coding-skills-d97aa0e48fdd)

### Templates / Boilerplates
* [Cookiecutter](https://github.com/audreyr/cookiecutter)
  * [Cookiecutter docs](https://cookiecutter.readthedocs.io/en/latest/readme.html)
  * [Cookiecutter templates](http://cookiecutter-templates.sebastianruml.name/)

# Functional programming
* [What is Functional Programming? Tutorial with Example](https://www.guru99.com/functional-programming-tutorial.html)

# History
* [Apollo's Code: Meet the Computer Programmer Who Landed Us on the Moon](https://www.wsj.com/articles/apollo-11-had-a-hidden-hero-software-11563153001?mod=rsswn)

# Varia
* [Zero Width Shortener](https://zws.im/)
* [TLDR This!](https://tldr.hackeryogi.com/) - Automatically summarize any article, text, document, webpage or essay in a click.
* [Memorize Terminal Commands](https://www.memorize-terminal-commands.com/)
* [Software Library: MS-DOS Games](https://archive.org/details/softwarelibrary_msdos_games?and%5B%5D=emulator_start%3A%2Aiafix%2A&sin=&sort=-publicdate)
* [pipedream](https://pipedream.com/) - Develop any workflow, based on any trigger. Workflows are code, which you can run for free. No server or cloud resources to manage.
* [Paged Out!](https://pagedout.institute/) - Paged Out! is a new experimental (one article == one page) free magazine about programming (especially programming tricks!), hacking, security hacking, retro computers, modern computers, electronics, demoscene, and other similar topics.
 
### Ideas, thoughts
* [How I side project](https://dotink.co/posts/how-i-side-project/)
* [My Text Editor Journey: Vim, Spacemacs, Atom and Sublime Text](http://thume.ca/2017/03/04/my-text-editor-journey-vim-spacemacs-atom-and-sublime-text/)
* [Why you should have a side project](https://erickhun.com/posts/why-you-should-have-a-side-project/)

